package com.kayalar.iftarvakti.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationReader {

	public Configurations getPropValues() throws IOException {

		Properties prop = new Properties();
		String propFilePath = "conf/config.properties";

		FileInputStream inputStream = new FileInputStream(propFilePath);

		prop.load(inputStream);

		Configurations config = new Configurations();

		config.setBotToken(prop.getProperty("bot.token"));
		config.setBotUserId(prop.getProperty("bot.user.id"));
		config.setDbAdress(prop.getProperty("db.address"));
		config.setDbName(prop.getProperty("db.name"));
		config.setDbPort(Integer.parseInt(prop.getProperty("db.port")));
		config.setApiToken(prop.getProperty("api.token"));
		config.setApiAddress(prop.getProperty("api.address"));

		inputStream.close();

		return config;
	}
}
