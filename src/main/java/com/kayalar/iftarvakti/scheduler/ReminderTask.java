package com.kayalar.iftarvakti.scheduler;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import com.kayalar.iftarvakti.http.DiyanetPrayTimesRequester;
import com.kayalar.iftarvakti.http.NotificationSender;
import com.kayalar.iftarvakti.model.DailyPrayTimes;
import com.kayalar.iftarvakti.model.FastEnum;
import com.kayalar.iftarvakti.model.RemainingTime;
import com.kayalar.iftarvakti.model.User;
import com.kayalar.iftarvakti.service.DatabaseManager;
import com.kayalar.iftarvakti.service.IftarVaktiHelper;

public class ReminderTask implements Runnable {

	private DatabaseManager dbManager;
	private NotificationSender notificationSender;

	private static final Logger logger = Logger.getLogger(com.kayalar.iftarvakti.scheduler.ReminderTask.class);

	public ReminderTask(DatabaseManager dbManager, NotificationSender notificationSender) {
		this.dbManager = dbManager;
		this.notificationSender = notificationSender;
	}

	@Override
	public void run() {

		try {

			int notifiedUserCount = 0;

			for (User user : dbManager.getUsers().values()) {

				Integer townId = user.getTownId();
				if (townId == null)
					continue;

				DailyPrayTimes todayInfo = dbManager.getTodaysPrayTimes(townId);

				if (todayInfo == null) {
					Map<String, DailyPrayTimes> prayTimes30Days = DiyanetPrayTimesRequester.requestForTown(townId);

					for (Entry<String, DailyPrayTimes> entry : prayTimes30Days.entrySet()) {
						String dayStr = entry.getKey();
						DailyPrayTimes prayTimes = entry.getValue();

						dbManager.saveDailyPrayTimes(dayStr, townId, prayTimes);
					}
				}
				todayInfo = dbManager.getTodaysPrayTimes(townId);
				DailyPrayTimes tomorrowInfo = dbManager.getTomorrowsPrayTimes(townId);

				RemainingTime rt = IftarVaktiHelper.getRemainingTime(todayInfo, tomorrowInfo);

				FastEnum fastEnum = rt.getFastEnum();
				ZonedDateTime pivot = rt.getTime();
				int remainingTimeInSec = rt.getRemainingDurationInSec();
				int remainingTimeInMin = remainingTimeInSec / 60;

				int timer;
				String message = IftarVaktiHelper.handsomeResultString(fastEnum, remainingTimeInSec, pivot.getHour(),
						pivot.getMinute(), user.getTownName());

				SendMessage sendMessage = new SendMessage(user.getChatId(), message);

				switch (fastEnum) {
				case IFTAR:
					timer = user.getIftarReminderTimer();
					if (user.isIftarReminderEnabled() && timer == remainingTimeInMin) {
						notificationSender.sendMessage(sendMessage);
						notifiedUserCount++;
					}
					break;
				case IMSAK:
					timer = user.getImsakReminderTimer();
					if (user.isImsakReminderEnabled() && timer == remainingTimeInMin) {
						notificationSender.sendMessage(sendMessage);
						notifiedUserCount++;
					}
					break;
				}
			}

			if (notifiedUserCount > 0)
				logger.info(String.format("%s user has been notified for their reminder", notifiedUserCount));

		} catch (Exception e) {
			logger.error("An error occured while sending notification to users", e);
		}

	}

}
