package com.kayalar.iftarvakti.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpException;
import org.apache.http.client.ClientProtocolException;

import com.kayalar.iftarvakti.IftarVaktiApp;
import com.kayalar.iftarvakti.http.DiyanetPrayTimesRequester;
import com.kayalar.iftarvakti.model.DailyPrayTimes;
import com.kayalar.iftarvakti.model.FastEnum;
import com.kayalar.iftarvakti.model.RemainingTime;

public class IftarVaktiHelper {

	public static DailyPrayTimes getDailyPrayTimes(DatabaseManager dbManager, Integer townId)
			throws NumberFormatException, ClientProtocolException, IOException, URISyntaxException, HttpException {

		DailyPrayTimes todayInfo = dbManager.getTodaysPrayTimes(townId);

		if (todayInfo == null) {

			Map<String, DailyPrayTimes> prayTimes30Days = DiyanetPrayTimesRequester.requestForTown(townId);

			for (Entry<String, DailyPrayTimes> entry : prayTimes30Days.entrySet()) {
				String dayStr = entry.getKey();
				DailyPrayTimes prayTimes = entry.getValue();

				dbManager.saveDailyPrayTimes(dayStr, townId, prayTimes);
			}
		}

		todayInfo = dbManager.getTodaysPrayTimes(townId);

		return todayInfo;
	}

	public static RemainingTime getRemainingTime(DailyPrayTimes todayPayTimes, DailyPrayTimes tomorrowPrayTimes) {
		// Time of now
		ZonedDateTime now = ZonedDateTime.now();

		String zoneId = "Europe/Istanbul";

		ZonedDateTime todayImsak = ZonedDateTime.of(todayPayTimes.getYear(), todayPayTimes.getMonth(),
				todayPayTimes.getDay(), todayPayTimes.getImsakHour(), todayPayTimes.getImsakMinute(), 0, 0,
				ZoneId.of(zoneId));
		ZonedDateTime todayIftar = ZonedDateTime.of(todayPayTimes.getYear(), todayPayTimes.getMonth(),
				todayPayTimes.getDay(), todayPayTimes.getAksamHour(), todayPayTimes.getAksamMinute(), 0, 0,
				ZoneId.of(zoneId));

		ZonedDateTime tomorrowImsak = ZonedDateTime.of(tomorrowPrayTimes.getYear(), tomorrowPrayTimes.getMonth(),
				tomorrowPrayTimes.getDay(), tomorrowPrayTimes.getImsakHour(), tomorrowPrayTimes.getImsakMinute(), 0, 0,
				ZoneId.of(zoneId));

		FastEnum fastEnum;
		ZonedDateTime pivot;

		if (now.isAfter(todayImsak) && now.isBefore(todayIftar)) {
			fastEnum = FastEnum.IFTAR;
			pivot = todayIftar;
		} else {
			fastEnum = FastEnum.IMSAK;
			if (now.isBefore(todayImsak))
				pivot = todayImsak;
			else {
				pivot = tomorrowImsak;
			}
		}

		int duration = (int) Math.abs(Duration.between(pivot, now).getSeconds());

		RemainingTime rt = new RemainingTime(fastEnum, pivot, duration);
		return rt;
	}

	public static String handsomeResultString(FastEnum fastEnum, int duration, int pivotHour, int pivotMinute,
			String cityName) {

		int remainingSecond = duration % 60;
		int temp = duration / 60;
		int remainingMinute = temp % 60;
		int remainingHour = temp / 60;

		String remainingStr = "";

		if (remainingHour != 0)
			remainingStr += (remainingHour + " saat ");

		if (remainingMinute != 0)
			remainingStr += (remainingMinute + " dakika ");

		if (remainingSecond != 0)
			remainingStr += (remainingSecond + " saniye");

		String pivotStr = "";

		if (pivotHour < 10)
			pivotStr += "0";
		pivotStr += pivotHour + ":";

		if (pivotMinute < 10)
			pivotStr += "0";
		pivotStr += pivotMinute;

		return String.format("%s\n%s için kalan süre:\n%s\n%s vakti: %s",
				IftarVaktiApp.clearTurkishChars(cityName.toUpperCase()), fastEnum, remainingStr, fastEnum, pivotStr);
	}
}
