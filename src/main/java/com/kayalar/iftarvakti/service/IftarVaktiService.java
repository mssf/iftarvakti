package com.kayalar.iftarvakti.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpException;
import org.apache.log4j.Logger;

import com.kayalar.iftarvakti.config.Configurations;
import com.kayalar.iftarvakti.http.NotificationSender;
import com.kayalar.iftarvakti.model.DailyPrayTimes;
import com.kayalar.iftarvakti.model.FastEnum;
import com.kayalar.iftarvakti.model.RemainingTime;
import com.kayalar.iftarvakti.model.TownSelectionProgress;
import com.kayalar.iftarvakti.model.User;
import com.kayalar.iftarvakti.scheduler.ReminderTask;

public class IftarVaktiService {

	private DatabaseManager dbManager;
	private NotificationSender notificationSender;

	private static final String errorMessage = "Beklenmeyen sonuç. Daha sonra tekrar deneyiniz.";

	private static final Logger logger = Logger.getLogger(com.kayalar.iftarvakti.service.IftarVaktiService.class);

	public IftarVaktiService(Configurations config) throws UnknownHostException {
		dbManager = new DatabaseManager(config.getDbAdress(), config.getDbPort(), config.getDbName());
		notificationSender = new NotificationSender(config.getBotToken());

		initReminderScheduler();
	}

	public String askForTown(Integer townId, String townName) {

		try {

			DailyPrayTimes todayPrayTimes = IftarVaktiHelper.getDailyPrayTimes(dbManager, townId);
			DailyPrayTimes tomorrowPrayTimes = dbManager.getTomorrowsPrayTimes(townId);

			RemainingTime rt = IftarVaktiHelper.getRemainingTime(todayPrayTimes, tomorrowPrayTimes);
			int duration = rt.getRemainingDurationInSec();
			FastEnum fastEnum = rt.getFastEnum();
			ZonedDateTime pivot = rt.getTime();

			return IftarVaktiHelper.handsomeResultString(fastEnum, duration, pivot.getHour(), pivot.getMinute(),
					townName);
		} catch (NumberFormatException | IOException | URISyntaxException | HttpException e) {
			logger.error("An error occured while requesting daily pray times.", e);
			return errorMessage;
		}
	}

	public void setCountryForUser(Long userId, Integer countryId) {
		dbManager.setCountryForUser(userId, countryId);
	}

	public void setCityForUser(Long userId, Integer cityId) {
		dbManager.setCityForUser(userId, cityId);
	}

	public void setTownForUser(Long userId, Integer countryId, Integer cityId, Integer townId, String townName) {
		dbManager.setTownForUser(userId, countryId, cityId, townId, townName);
	}

	public void disableIftarReminderForUser(Long userId) {
		dbManager.disableIftarReminderForUser(userId);
	}

	public void disableImsakReminderForUser(Long userId) {
		dbManager.disableImsakReminderForUser(userId);
	}

	public void setIftarReminderForUser(Long userId, int reminderTime) {
		dbManager.setIftarReminderForUser(userId, reminderTime);
	}

	public void setImsakReminderForUser(Long userId, int reminderTime) {
		dbManager.setImsakReminderForUser(userId, reminderTime);
	}

	public void setLastActivityForUser(Long userId, String prevMessage) {
		dbManager.setLastActivityForUser(userId, prevMessage);
	}

	public void setTownSelectionProgressForUser(Long userId, TownSelectionProgress progress) {
		dbManager.setIsTownSelectionProgressForUser(userId, progress);
	}

	public User getUser(Long userId) {
		return dbManager.getUser(userId);
	}

	public void addUser(User user) {
		dbManager.addUser(user);
	}

	public Map<Long, User> getUsers() {
		return dbManager.getUsers();
	}

	public Set<String> getCountryNames() {
		return dbManager.getCountryNames();
	}

	public Set<String> getCityNames(int countryId) {
		return dbManager.getCityNames(countryId);
	}

	public Set<String> getTownNames(int cityId) {
		return dbManager.getTownNames(cityId);
	}

	public Map<String, Integer> getCountryNameMap() {
		return dbManager.getCountryNameMap();
	}

	public Map<String, Integer> getCityNameMap(int countryId) {
		return dbManager.getCityNameMap(countryId);
	}

	public Map<String, Integer> getTownNameMap(int cityId) {
		return dbManager.getTownNameMap(cityId);
	}

	public Set<Integer> getCountryIds() {
		return dbManager.getCountryIds();
	}

	public Set<Integer> getCityIds(int countryId) {
		return dbManager.getCityIds(countryId);
	}

	public Set<Integer> getTownIds(int cityId) {
		return dbManager.getTownIds(cityId);
	}

	private void initReminderScheduler() {

		ReminderTask reminderTask = new ReminderTask(dbManager, notificationSender);

		ZonedDateTime now = ZonedDateTime.now();
		int second = now.getSecond();
		int delay = 60 - second;

		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(reminderTask, delay, 60, TimeUnit.SECONDS);
	}
}
