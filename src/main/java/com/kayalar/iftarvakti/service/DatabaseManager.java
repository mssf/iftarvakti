package com.kayalar.iftarvakti.service;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.kayalar.iftarvakti.model.City;
import com.kayalar.iftarvakti.model.Country;
import com.kayalar.iftarvakti.model.DailyPrayTimes;
import com.kayalar.iftarvakti.model.DailyPrayTimesTownMap;
import com.kayalar.iftarvakti.model.Town;
import com.kayalar.iftarvakti.model.TownSelectionProgress;
import com.kayalar.iftarvakti.model.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.util.JSON;

public class DatabaseManager {

	private DBCollection prayTimesTable;
	private DBCollection userTable;
	private DBCollection countryTable;
	private DBCollection cityTable;
	private DBCollection townTable;

	private Gson gson;

	private static final Logger logger = Logger.getLogger(com.kayalar.iftarvakti.service.DatabaseManager.class);

	private static final String PRAY_TIMES_TABLE_NAME = "pray_times";
	private static final String USER_TABLE_NAME = "user";
	private static final String COUNTRY_TABLE_NAME = "country";
	private static final String CITY_TABLE_NAME = "city";
	private static final String TOWN_TABLE_NAME = "town";

	public DatabaseManager(String dbAdress, int dbPort, String dbName) {
		MongoClient mongoClient = new MongoClient(
				new MongoClientURI(String.format("mongodb://%s:%s", dbAdress, dbPort)));

		DB database = mongoClient.getDB(dbName);

		gson = new Gson();

		prayTimesTable = database.getCollection(PRAY_TIMES_TABLE_NAME);
		userTable = database.getCollection(USER_TABLE_NAME);
		countryTable = database.getCollection(COUNTRY_TABLE_NAME);
		cityTable = database.getCollection(CITY_TABLE_NAME);
		townTable = database.getCollection(TOWN_TABLE_NAME);
	}

	public DailyPrayTimes getTodaysPrayTimes(Integer townId) {
		String currentDayStr = dateStr(ZonedDateTime.now());
		return getPrayTimesForTown(currentDayStr, townId);
	}

	public DailyPrayTimes getTomorrowsPrayTimes(Integer townId) {
		String currentDayStr = dateStr(ZonedDateTime.now().plusDays(1));
		return getPrayTimesForTown(currentDayStr, townId);
	}

	public void saveDailyPrayTimes(String dayStr, Integer townId, DailyPrayTimes prayTimes) {
		DailyPrayTimesTownMap prayTimesTownMap = getPrayTimesTownMap(dayStr, townId);

		if (prayTimesTownMap == null) {
			prayTimesTownMap = new DailyPrayTimesTownMap(dayStr);
		}

		prayTimesTownMap.addDailyPrayTimes(townId, prayTimes);

		BasicDBObject query = new BasicDBObject();
		query.put("dayStr", dayStr);

		BasicDBObject obj = (BasicDBObject) JSON.parse(gson.toJson(prayTimesTownMap));
		prayTimesTable.update(query, obj, true, false);// if exists update, if not insert
	}

	public Map<Long, User> getUsers() {
		Map<Long, User> userMap = new HashMap<Long, User>();

		DBCursor dbCursor = userTable.find();

		while (dbCursor.hasNext()) {
			DBObject obj = dbCursor.next();
			User user = gson.fromJson(JSON.serialize(obj), User.class);
			userMap.put(user.getUserId(), user);
		}

		return userMap;
	}

	public Set<String> getCountryNames() {
		Set<String> countrySet = new LinkedHashSet<String>();

		DBCursor dbCursor = countryTable.find();
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("countryNameEn", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			Country country = gson.fromJson(JSON.serialize(obj), Country.class);
			countrySet.add(country.getCountryName());
		}

		return countrySet;
	}

	public Set<String> getCityNames(Integer countryId) {
		Set<String> citySet = new LinkedHashSet<String>();

		BasicDBObject query = new BasicDBObject();
		query.put("countryId", countryId);

		DBCursor dbCursor = cityTable.find(query);
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("cityId", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			City city = gson.fromJson(JSON.serialize(obj), City.class);
			citySet.add(city.getCityName());
		}

		return citySet;
	}

	public Set<String> getTownNames(Integer cityId) {
		Set<String> townSet = new LinkedHashSet<String>();

		BasicDBObject query = new BasicDBObject();
		query.put("cityId", cityId);

		DBCursor dbCursor = townTable.find(query);
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("townNameEn", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			Town town = gson.fromJson(JSON.serialize(obj), Town.class);
			townSet.add(town.getTownName());
		}

		return townSet;
	}

	public Map<String, Integer> getCountryNameMap() {
		Map<String, Integer> countryMap = new LinkedHashMap<String, Integer>();

		DBCursor dbCursor = countryTable.find();
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("countryNameEn", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			Country country = gson.fromJson(JSON.serialize(obj), Country.class);
			countryMap.put(country.getCountryName(), country.getCountryId());
		}

		return countryMap;
	}

	public Map<String, Integer> getCityNameMap(Integer countryId) {
		Map<String, Integer> cityMap = new LinkedHashMap<String, Integer>();

		BasicDBObject query = new BasicDBObject();
		query.put("countryId", countryId);

		DBCursor dbCursor = cityTable.find(query);
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("cityId", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			City city = gson.fromJson(JSON.serialize(obj), City.class);
			cityMap.put(city.getCityName(), city.getCityId());
		}

		return cityMap;
	}

	public Map<String, Integer> getTownNameMap(Integer cityId) {
		Map<String, Integer> townMap = new LinkedHashMap<String, Integer>();

		BasicDBObject query = new BasicDBObject();
		query.put("cityId", cityId);

		DBCursor dbCursor = townTable.find(query);
		BasicDBObject sortQuery = new BasicDBObject();
		sortQuery.put("townNameEn", 1);
		DBCursor sortedCursor = dbCursor.sort(sortQuery);

		while (sortedCursor.hasNext()) {
			DBObject obj = sortedCursor.next();
			Town town = gson.fromJson(JSON.serialize(obj), Town.class);
			townMap.put(town.getTownName(), town.getTownId());
		}

		return townMap;
	}

	public Set<Integer> getCountryIds() {
		Set<Integer> countrySet = new HashSet<Integer>();

		DBCursor dbCursor = countryTable.find();

		while (dbCursor.hasNext()) {
			DBObject obj = dbCursor.next();
			Country country = gson.fromJson(JSON.serialize(obj), Country.class);
			countrySet.add(country.getCountryId());
		}

		return countrySet;
	}

	public Set<Integer> getCityIds(Integer countryId) {
		Set<Integer> citySet = new HashSet<Integer>();

		BasicDBObject query = new BasicDBObject();
		query.put("countryId", countryId);

		DBCursor dbCursor = cityTable.find(query);

		while (dbCursor.hasNext()) {
			DBObject obj = dbCursor.next();
			City city = gson.fromJson(JSON.serialize(obj), City.class);
			citySet.add(city.getCityId());
		}

		return citySet;
	}

	public Set<Integer> getTownIds(Integer cityId) {
		Set<Integer> townSet = new HashSet<Integer>();

		BasicDBObject query = new BasicDBObject();
		query.put("cityId", cityId);

		DBCursor dbCursor = townTable.find(query);

		while (dbCursor.hasNext()) {
			DBObject obj = dbCursor.next();
			Town town = gson.fromJson(JSON.serialize(obj), Town.class);
			townSet.add(town.getTownId());
		}

		return townSet;
	}

	public void addUser(User user) {
		BasicDBObject obj = (BasicDBObject) JSON.parse(gson.toJson(user));
		userTable.insert(obj);
	}

	public void setCountryForUser(Long userId, Integer countryId) {
		User user = getUser(userId);
		user.setCountryId(countryId);
		updateUserInDB(user);
	}

	public void setCityForUser(Long userId, Integer cityId) {
		User user = getUser(userId);
		user.setCityId(cityId);
		updateUserInDB(user);
	}

	public void setTownForUser(Long userId, Integer countryId, Integer cityId, Integer townId, String townName) {
		User user = getUser(userId);
		user.setCountryId(countryId);
		user.setCityId(cityId);
		user.setTownId(townId);
		user.setTownName(townName);
		updateUserInDB(user);
	}

	public void disableIftarReminderForUser(Long userId) {
		User user = getUser(userId);
		user.disableIftarReminder();
		updateUserInDB(user);
	}

	public void disableImsakReminderForUser(Long userId) {
		User user = getUser(userId);
		user.disableImsakReminder();
		updateUserInDB(user);
	}

	public void setIftarReminderForUser(Long userId, int reminderTime) {
		User user = getUser(userId);
		user.setIftarReminderTimer(reminderTime);
		updateUserInDB(user);
	}

	public void setImsakReminderForUser(Long userId, int reminderTime) {
		User user = getUser(userId);
		user.setImsakReminderTimer(reminderTime);
		updateUserInDB(user);
	}

	public void setLastActivityForUser(Long userId, String message) {
		User user = getUser(userId);
		user.setLastActivity(message);
		updateUserInDB(user);
	}

	public void setIsTownSelectionProgressForUser(Long userId, TownSelectionProgress progress) {
		User user = getUser(userId);
		user.setTownSelectionProgress(progress);
		updateUserInDB(user);
	}

	private void updateUserInDB(User user) {
		BasicDBObject query = new BasicDBObject();
		query.put("userId", user.getUserId());

		BasicDBObject obj = (BasicDBObject) JSON.parse(gson.toJson(user));
		userTable.update(query, obj, false, false);

		logger.info(String.format("User=%s has been updated in database", user.getUserId()));
	}

	public User getUser(Long userId) {
		Map<Long, User> userMap = getUsers();
		return userMap.get(userId);
	}

	private DailyPrayTimesTownMap getPrayTimesTownMap(String dayStr, Integer townId) {
		BasicDBObject query = new BasicDBObject();
		query.put("dayStr", dayStr);

		DBCursor dbCursor = prayTimesTable.find(query);

		if (!dbCursor.hasNext()) {
			logger.info(String.format("There is no pray times available for day=%s", dayStr));
			return null;
		} else {

			// we expect only one document
			DBObject obj = dbCursor.next();
			DailyPrayTimesTownMap prayTimesTownMap = gson.fromJson(JSON.serialize(obj), DailyPrayTimesTownMap.class);

			return prayTimesTownMap;
		}
	}

	private DailyPrayTimes getPrayTimesForTown(String dayStr, Integer townId) {

		DailyPrayTimesTownMap prayTimesTownMap = getPrayTimesTownMap(dayStr, townId);

		if (prayTimesTownMap == null) {
			return null;
		}

		DailyPrayTimes prayTimes = prayTimesTownMap.getDailyPrayTimes(townId);

		if (prayTimes == null) {
			logger.info(String.format("There is no pray times available for town=%s day=%s", townId, dayStr));
			return null;
		}

		return prayTimes;
	}

	private String dateStr(ZonedDateTime zdt) {
		return DateTimeFormatter.ofPattern("yyyMMdd").format(zdt);
	}
}
