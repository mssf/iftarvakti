package com.kayalar.iftarvakti.model;

import java.util.HashMap;
import java.util.Map;

public class DailyPrayTimesTownMap {

	private Map<Integer, DailyPrayTimes> townMap;
	private String dayStr;

	public DailyPrayTimesTownMap(String str) {
		dayStr = str;
		townMap = new HashMap<Integer, DailyPrayTimes>();
	}

	public DailyPrayTimes getDailyPrayTimes(Integer townId) {
		return townMap.get(townId);
	}

	public void addDailyPrayTimes(Integer townId, DailyPrayTimes dailyPrayTimes) {
		townMap.put(townId, dailyPrayTimes);
	}

	public void removeDailyPrayTimes(Integer townId) {
		townMap.remove(townId);
	}

	public String getDayStr() {
		return dayStr;
	}
}
