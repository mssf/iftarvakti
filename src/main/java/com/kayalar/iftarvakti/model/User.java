package com.kayalar.iftarvakti.model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

public class User {

	private Long userId;
	private String firstName;
	private String userName;
	private String lastName;
	private Long chatId;
	private Integer iftarReminderTimer;
	private Integer imsakReminderTimer;
	private Integer activityCount;
	private List<AbstractMap.SimpleEntry<String, String>> previousMessages;
	private Integer countryId;
	private Integer cityId;
	private Integer townId;
	private String townName;
	private TownSelectionProgress townSelectionProgress;

	public User(Long userId, Long chatId, String firstName, String lastName, String userName) {

		this.userId = userId;
		this.firstName = firstName;
		this.userName = userName;
		this.lastName = lastName;
		this.chatId = chatId;
		this.iftarReminderTimer = 0;
		this.imsakReminderTimer = 0;
		this.activityCount = 1;
		this.previousMessages = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
		setLastActivity("/start");
		this.townName = "";
		this.townSelectionProgress = TownSelectionProgress.TO_SELECT_COUNTRY;
	}

	public User(Long userId, String firstName, String userName, String lastName, Long chatId,
			Integer iftarReminderTimer, Integer imsakReminderTimer, Integer activityCount,
			List<AbstractMap.SimpleEntry<String, String>> previousMessage, Integer countryId, Integer cityId,
			Integer townId, String townName) {
		this.userId = userId;
		this.firstName = firstName;
		this.userName = userName;
		this.lastName = lastName;
		this.chatId = chatId;
		this.iftarReminderTimer = iftarReminderTimer;
		this.imsakReminderTimer = imsakReminderTimer;
		this.activityCount = activityCount;
		this.previousMessages = previousMessage;
		this.countryId = countryId;
		this.cityId = cityId;
		this.townId = townId;
		this.townName = townName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getChatId() {
		return chatId;
	}

	public void setChatId(Long chatId) {
		this.chatId = chatId;
	}

	public Integer getIftarReminderTimer() {
		return iftarReminderTimer;
	}

	public void setIftarReminderTimer(Integer iftarReminderTimer) {
		this.iftarReminderTimer = iftarReminderTimer;
	}

	public void disableIftarReminder() {
		this.iftarReminderTimer = 0;
	}

	public boolean isIftarReminderEnabled() {
		return iftarReminderTimer != 0;
	}

	public Integer getImsakReminderTimer() {
		return imsakReminderTimer;
	}

	public void setImsakReminderTimer(Integer imsakReminderTimer) {
		this.imsakReminderTimer = imsakReminderTimer;
	}

	public void disableImsakReminder() {
		this.imsakReminderTimer = 0;
	}

	public boolean isImsakReminderEnabled() {
		return imsakReminderTimer != 0;
	}

	public Integer getActivityCount() {
		return activityCount;
	}

	public void setActivityCount(Integer activityCount) {
		this.activityCount = activityCount;
	}

	public List<AbstractMap.SimpleEntry<String, String>> getPreviousMessages() {
		return previousMessages;
	}

	public List<String> getLastThreePreviousMessages() {

		ArrayList<String> lastMessages = new ArrayList<String>();

		String lastMessage, secondLastMessage, thirdLastMessage;

		if (previousMessages == null || previousMessages.size() == 0) {
			lastMessage = "";
			secondLastMessage = "";
			thirdLastMessage = "";
		}

		else if (previousMessages.size() == 1) {
			lastMessage = previousMessages.get(previousMessages.size() - 1).getValue();
			secondLastMessage = "";
			thirdLastMessage = "";
		}

		else if (previousMessages.size() == 2) {
			lastMessage = previousMessages.get(previousMessages.size() - 1).getValue();
			secondLastMessage = previousMessages.get(previousMessages.size() - 2).getValue();
			thirdLastMessage = "";
		}

		else {
			lastMessage = previousMessages.get(previousMessages.size() - 1).getValue();
			secondLastMessage = previousMessages.get(previousMessages.size() - 2).getValue();
			thirdLastMessage = previousMessages.get(previousMessages.size() - 3).getValue();
		}

		lastMessages.add(lastMessage);
		lastMessages.add(secondLastMessage);
		lastMessages.add(thirdLastMessage);

		return lastMessages;
	}

	public void setPreviousMessages(List<AbstractMap.SimpleEntry<String, String>> previousMessages) {
		this.previousMessages = previousMessages;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getTownId() {
		return townId;
	}

	public void setTownId(Integer townId) {
		this.townId = townId;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public TownSelectionProgress getTownSelectionProgress() {
		return townSelectionProgress;
	}

	public void setTownSelectionProgress(TownSelectionProgress townSelectionProgress) {
		this.townSelectionProgress = townSelectionProgress;
	}

	public void setLastActivity(String previousMessage) {
		String nowStr = DateTimeFormatter.ofPattern("yyyMMdd-hh:mm").format(ZonedDateTime.now());
		SimpleEntry<String, String> entry = new SimpleEntry<String, String>(nowStr, previousMessage);

		if (previousMessages == null) {
			previousMessages = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
		}

		previousMessages.add(entry);
		activityCount++;
	}
}
