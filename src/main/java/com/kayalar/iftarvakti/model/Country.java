package com.kayalar.iftarvakti.model;

public class Country {

	private String countryName;
	private String countryNameEn;
	private Integer countryId;

	public Country(String countryName, Integer countryId) {
		this.countryName = countryName;
		this.countryId = countryId;
	}

	public Country(String countryName, String countryNameEn, Integer countryId) {
		this.countryName = countryName;
		this.countryNameEn = countryNameEn;
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getCountryNameEn() {
		return countryNameEn;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryNameEn(String countryNameEn) {
		this.countryNameEn = countryNameEn;
	}

	@Override
	public String toString() {
		return "Country [countryName=" + countryName + ", countryNameEn=" + countryNameEn + ", countryId=" + countryId
				+ "]";
	}
}
