package com.kayalar.iftarvakti.model;

public class City {

	private String cityName;
	private String cityNameEn;
	private Integer cityId;
	private Integer countryId;

	public City(String cityName, Integer cityId, Integer countryId) {
		this.cityName = cityName;
		this.cityId = cityId;
		this.countryId = countryId;
	}

	public City(String cityName, String cityNameEn, Integer cityId, Integer countryId) {
		this.cityName = cityName;
		this.cityNameEn = cityNameEn;
		this.cityId = cityId;
		this.countryId = countryId;
	}

	public String getCityName() {
		return cityName;
	}

	public String getCityNameEn() {
		return cityNameEn;
	}

	public Integer getCityId() {
		return cityId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCityNameEn(String cityNameEn) {
		this.cityNameEn = cityNameEn;
	}

	@Override
	public String toString() {
		return "City [cityName=" + cityName + ", cityNameEn=" + cityNameEn + ", cityId=" + cityId + ", countryId="
				+ countryId + "]";
	}
}
