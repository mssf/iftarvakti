package com.kayalar.iftarvakti.model;

import java.util.HashMap;
import java.util.Map;

public class MultiDayPrayTimesTownMap {

	private Map<String, DailyPrayTimesTownMap> multiDayTownMap;

	public MultiDayPrayTimesTownMap() {
		multiDayTownMap = new HashMap<String, DailyPrayTimesTownMap>();
	}

	public DailyPrayTimesTownMap getTownMap(String dayStr) {
		return multiDayTownMap.get(dayStr);
	}

	public void addTownMap(String dayStr, DailyPrayTimesTownMap dailyPrayTimesTownMap) {
		multiDayTownMap.put(dayStr, dailyPrayTimesTownMap);
	}

	public void removeTownMap(String dayStr) {
		multiDayTownMap.remove(dayStr);
	}

	public DailyPrayTimes getDailyPrayTimes(String dayStr, Integer townId) {
		DailyPrayTimesTownMap dailyPrayTimesTownMap = multiDayTownMap.get(dayStr);

		if (dailyPrayTimesTownMap == null)
			return null;

		return dailyPrayTimesTownMap.getDailyPrayTimes(townId);
	}

	public void addDailyPrayTimes(String dayStr, Integer townId, DailyPrayTimes dailyPrayTimes) {
		DailyPrayTimesTownMap dailyPrayTimesTownMap = multiDayTownMap.get(dayStr);
		dailyPrayTimesTownMap.addDailyPrayTimes(townId, dailyPrayTimes);
	}

	public void removeDailyPrayTimes(String dayStr, Integer townId) {
		DailyPrayTimesTownMap dailyPrayTimesTownMap = multiDayTownMap.get(dayStr);
		dailyPrayTimesTownMap.removeDailyPrayTimes(townId);
	}
}
