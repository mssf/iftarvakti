package com.kayalar.iftarvakti.model;

public class Town {

	private String townName;
	private String townNameEn;
	private Integer townId;
	private Integer cityId;
	private Integer countryId;

	public Town(String townName, Integer townId, Integer cityId, Integer countryId) {
		this.townName = townName;
		this.townId = townId;
		this.cityId = cityId;
		this.countryId = countryId;
	}

	public Town(String townName, String townNameEn, Integer townId, Integer cityId, Integer countryId) {
		this.townName = townName;
		this.townNameEn = townNameEn;
		this.townId = townId;
		this.cityId = cityId;
		this.countryId = countryId;
	}

	public String getTownName() {
		return townName;
	}

	public String getTownNameEn() {
		return townNameEn;
	}

	public Integer getTownId() {
		return townId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setTownNameEn(String townNameEn) {
		this.townNameEn = townNameEn;
	}

	@Override
	public String toString() {
		return "Town [townName=" + townName + ", townNameEn=" + townNameEn + ", townId=" + townId + ", cityId=" + cityId
				+ ", countryId=" + countryId + "]";
	}
}
