package com.kayalar.iftarvakti.telegram;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import com.kayalar.iftarvakti.IftarVaktiApp;
import com.kayalar.iftarvakti.config.ConfigurationReader;
import com.kayalar.iftarvakti.config.Configurations;
import com.kayalar.iftarvakti.http.NotificationSender;
import com.kayalar.iftarvakti.model.TownSelectionProgress;
import com.kayalar.iftarvakti.model.User;
import com.kayalar.iftarvakti.service.IftarVaktiService;

import net.ricecode.similarity.JaroWinklerStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;

public class IftarVaktiBot extends TelegramLongPollingBot {

	private static final String SAVE_TOWN_COMMAND = "Şehir Kaydet";
	private static final String ASK_REMAINING_TIME_COMMAND = "Ne Kadar Kaldı?";
	private static final String SET_IFTAR_REMINDER_COMMAND = "İftar İçin Hatırlatıcı Ayarla";
	private static final String SET_IMSAK_REMINDER_COMMAND = "İmsak İçin Hatırlatıcı Ayarla";
	private static final String DISABLE_IFTAR_REMINDER_COMMAND = "İftar İçin Hatırlatıcı Kapat";
	private static final String DISABLE_IMSAK_REMINDER_COMMAND = "İmsak İçin Hatırlatıcı Kapat";
	private static final String HELP_COMMAND = "Yardım";
	private static final String BACK_ARROW_COMMAND = "\u2B05 Geri";

	private static final String NOT_SAVED_TOWN_ERROR = String.format(
			"Henüz şehirinizi kaydetmemişsiniz. %s komutu ile şehirinizi kaydedebilirsiniz. Bilgi için %s",
			SAVE_TOWN_COMMAND, HELP_COMMAND);
	private static final String WELCOME_MESSAGE = String
			.format("Hoşgeldiniz! Aşağıdaki  %s butonuna basarak bot hakkında bilgi alabilirsiniz.", HELP_COMMAND);

	private IftarVaktiService service;

	private Configurations config;

	private StringSimilarityService similarityService;

	private NotificationSender notificationService;

	private static final Logger logger = Logger.getLogger(com.kayalar.iftarvakti.telegram.IftarVaktiBot.class);

	public IftarVaktiBot() throws IOException {

		this.config = new ConfigurationReader().getPropValues();
		service = new IftarVaktiService(config);

		SimilarityStrategy strategy = new JaroWinklerStrategy();
		similarityService = new StringSimilarityServiceImpl(strategy);
		notificationService = new NotificationSender(getBotToken());

		Properties prop = new Properties();
		String propFilePath = "announcement/announcement.properties";
		File file = new File(propFilePath);

		if (file.exists()) {

			FileInputStream inputStream = new FileInputStream(propFilePath);
			prop.load(inputStream);
			String announcementText = prop.getProperty("message");

			for (User user : service.getUsers().values()) {
				SendMessage sendMsg = new SendMessage();
				sendMsg.setChatId(user.getChatId());
				sendMsg.setText(announcementText);
				sendMsg.setReplyMarkup(mainKeyboard());
				notificationService.sendMessage(sendMsg);
			}

			logger.info("Announcement made to users");
		} else {
			logger.info("There is no announcement");
		}

		logger.info("Bot started to run");
	}

	public void onUpdateReceived(Update update) {

		Message comingMessage = update.getMessage();
		String userCurrentMessageText = comingMessage.getText();

		org.telegram.telegrambots.meta.api.objects.User telegramUser = comingMessage.getFrom();
		Long userId = telegramUser.getId().longValue();
		Long chatId = comingMessage.getChatId();

		SendMessage sendMessage = new SendMessage();

		sendMessage.setReplyMarkup(mainKeyboard());

		logger.info(String.format("A message has arrived from userId=%s with text=%s", userId, userCurrentMessageText));
		User user = service.getUser(userId);
		if (user == null) {
			user = new User(userId, chatId, telegramUser.getFirstName(), telegramUser.getLastName(),
					telegramUser.getUserName());
			service.addUser(user);
			logger.info(String.format(
					"User=%s has been registered to service since this is his first time with the bot", userId));
		}

		List<String> userLastMessages = user.getLastThreePreviousMessages();
		String userLastMessage = userLastMessages.get(0);
		String userSecondLastMessage = userLastMessages.get(1);
		String userThirdLastMessage = userLastMessages.get(2);

		service.setLastActivityForUser(userId, userCurrentMessageText);

		if (userCurrentMessageText.equals("/start")) {
			sendMessage.setText(WELCOME_MESSAGE);
		}

		else if (userCurrentMessageText.equals(HELP_COMMAND) || userCurrentMessageText.toLowerCase().equals("yardim")
				|| userCurrentMessageText.toLowerCase().equals("yardım")) {
			String str1 = "1) İftar bilgisini öğrenmek istediğiniz şehirin, ilçenin adını yazıp gönderebilirsiniz. Ör:Ankara, Meram vb.";
			String str2 = String.format("2) Bulunduğunuz şehiri %s komutunu kullanarak kaydedebilirsiniz",
					SAVE_TOWN_COMMAND);
			String str3 = String.format(
					"3) %s komutuyla kaydettiğiniz şehir için iftara ya da imsaka ne kadar süre kaldığını öğrenebilirsiniz",
					ASK_REMAINING_TIME_COMMAND);
			String str4 = String.format(
					"4) %s komutuyla iftar vakitlerinden belirli vakit önce size otomatik hatırlatma mesajı gelmesini sağlayabilirsiniz.",
					SET_IFTAR_REMINDER_COMMAND);
			String str5 = String.format(
					"5) %s komutuyla imsak vakitlerinden belirli vakit önce size otomatik hatırlatma mesajı gelmesini sağlayabilirsiniz.",
					SET_IMSAK_REMINDER_COMMAND);
			String str6 = String.format("6) %s komutuyla ayarladığınız hatırlatıcıyı kapatabilirsiniz.",
					DISABLE_IFTAR_REMINDER_COMMAND);
			String str7 = String.format("7) %s komutuyla ayarladığınız hatırlatıcıyı kapatabilirsiniz.",
					DISABLE_IMSAK_REMINDER_COMMAND);

			sendMessage.setText(
					String.format("%s\n\n%s\n\n%s\n\n%s\n\n%s\n\n%s", str1, str2, str3, str4, str5, str6, str7));
		}

		else if (userCurrentMessageText.startsWith(SAVE_TOWN_COMMAND)) {

			Map<String, Integer> countryNameMap = service.getCountryNameMap();
			sendMessage.setReplyMarkup(townSelectionKeyboard(countryNameMap.keySet()));
			sendMessage.setText("Ülkenizi aşağıdan seçiniz ya da yazınız");
			service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_COUNTRY);
		}

		else if (userCurrentMessageText.equals(BACK_ARROW_COMMAND)) {
			service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_COUNTRY);
			sendMessage.setReplyMarkup(mainKeyboard());
			sendMessage.setText("Şehir seçimi tamamlanmadı");
		}

		else if (user.getTownSelectionProgress() == TownSelectionProgress.TO_SELECT_COUNTRY
				&& userLastMessage.equals(SAVE_TOWN_COMMAND)) {

			Map<String, Integer> countryNameMap = service.getCountryNameMap();
			Integer countryId = countryNameMap.get(userCurrentMessageText);

			if (countryId == null) {

				String clearedUserMessage = IftarVaktiApp.clearTurkishChars(userCurrentMessageText);

				for (Entry<String, Integer> countryEntry : countryNameMap.entrySet()) {
					double score = similarityService.score(clearedUserMessage, countryEntry.getKey());

					if (score > 0.9) {
						countryId = countryEntry.getValue();
						break;
					}
				}
			}

			if (countryId == null) {
				sendMessage.setText(String.format("%s isimli ülke bulunamadı", userCurrentMessageText));
				service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_COUNTRY);
			} else {

				Map<String, Integer> cityNameMap = service.getCityNameMap(countryId);
				sendMessage.setReplyMarkup(townSelectionKeyboard(cityNameMap.keySet()));
				sendMessage.setText("Şehrinizi aşağıdan seçiniz ya da yazınız");
				service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_CITY);
			}
		}

		else if (user.getTownSelectionProgress() == TownSelectionProgress.TO_SELECT_CITY
				&& userSecondLastMessage.equals(SAVE_TOWN_COMMAND)) {

			String countryName = userLastMessage;

			Map<String, Integer> countryNameMap = service.getCountryNameMap();
			Integer countryId = countryNameMap.get(countryName);
			Map<String, Integer> cityNameMap = service.getCityNameMap(countryId);

			Integer cityId = cityNameMap.get(userCurrentMessageText);

			if (cityId == null) {

				String clearedUserMessage = IftarVaktiApp.clearTurkishChars(userCurrentMessageText);

				for (Entry<String, Integer> cityEntry : cityNameMap.entrySet()) {
					double score = similarityService.score(clearedUserMessage, cityEntry.getKey());

					if (score > 0.9) {
						cityId = cityEntry.getValue();
						break;
					}
				}
			}

			if (cityId == null) {

				sendMessage.setText(String.format("%s isimli şehir bulunamadı", userCurrentMessageText));
				service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_COUNTRY);
			} else {

				Map<String, Integer> townNameMap = service.getTownNameMap(cityId);
				sendMessage.setReplyMarkup(townSelectionKeyboard(townNameMap.keySet()));
				sendMessage.setText("İlçenizi aşağıdan seçiniz ya da yazınız");
				service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_TOWN);
			}
		}

		else if (user.getTownSelectionProgress() == TownSelectionProgress.TO_SELECT_TOWN
				&& userThirdLastMessage.equals(SAVE_TOWN_COMMAND)) {

			String countryName = userSecondLastMessage;
			Map<String, Integer> countryNameMap = service.getCountryNameMap();
			Integer countryId = countryNameMap.get(countryName);
			Map<String, Integer> cityNameMap = service.getCityNameMap(countryId);
			String cityName = userLastMessage;
			Integer cityId = cityNameMap.get(cityName);
			Map<String, Integer> townNameMap = service.getTownNameMap(cityId);
			Integer townId = townNameMap.get(userCurrentMessageText);

			if (townId == null) {

				String clearedUserMessage = IftarVaktiApp.clearTurkishChars(userCurrentMessageText);

				for (Entry<String, Integer> townEntry : townNameMap.entrySet()) {
					double score = similarityService.score(clearedUserMessage, townEntry.getKey());

					if (score > 0.9) {
						townId = townEntry.getValue();
						break;
					}
				}
			}

			if (townId == null) {
				sendMessage.setText(String.format("%s isimli ilçe bulunamadı", userCurrentMessageText));
			} else {
				service.setTownForUser(userId, countryId, cityId, townId, userCurrentMessageText);
				sendMessage.setText(
						String.format("%s bulunduğunuz ilçe olarak sisteme kaydedildi.", userCurrentMessageText));
				logger.info(String.format("User=%s has been saved city=%s as his city", userId, cityName));
			}

			sendMessage.setReplyMarkup(mainKeyboard());
			service.setTownSelectionProgressForUser(userId, TownSelectionProgress.TO_SELECT_COUNTRY);
		}

		else if (userCurrentMessageText.equals(ASK_REMAINING_TIME_COMMAND)) {

			Integer townId = user.getTownId();
			String townName = user.getTownName();

			if (townId == null) {
				sendMessage.setText(NOT_SAVED_TOWN_ERROR);
			} else {
				String askResult = service.askForTown(townId, townName);
				sendMessage.setText(askResult);
			}
		}

		else if (userCurrentMessageText.equals(DISABLE_IFTAR_REMINDER_COMMAND)) {
			if (user.isIftarReminderEnabled()) {
				service.disableIftarReminderForUser(userId);
				logger.info(String.format("User=%s has been disabled his reminder for iftar", userId));
				sendMessage.setText("İftar hatırlatıcınız kapatıldı");
			} else {
				sendMessage.setText("İftar hatırlatıcınız zaten kapalı");
			}
		}

		else if (userCurrentMessageText.equals(DISABLE_IMSAK_REMINDER_COMMAND)) {
			if (user.isImsakReminderEnabled()) {
				service.disableImsakReminderForUser(userId);
				logger.info(String.format("User=%s has been disabled his reminder for imsak", userId));
				sendMessage.setText("İmsak hatırlatıcınız kapatıldı");
			} else {
				sendMessage.setText("İmsak hatırlatıcınız zaten kapalı");
			}
		}

		else if (userCurrentMessageText.equals(SET_IFTAR_REMINDER_COMMAND)) {

			if (user.getTownId() == null) {
				sendMessage.setText(NOT_SAVED_TOWN_ERROR);
			} else {
				sendMessage.setReplyMarkup(timeSelectionKeyboard());
				sendMessage.setText(
						"İftarlardan kaç dakika önce hatırlatılmak istediğinizi aşağıdan seçiniz ya da sayıyla yazınız");
			}
		}

		else if (userLastMessage.equals(SET_IFTAR_REMINDER_COMMAND)) {
			try {
				int reminderTime = getReminderTime(userCurrentMessageText);

				if (reminderTime > 0) {
					service.setIftarReminderForUser(userId, reminderTime);
					logger.info(String.format("User=%s has been enabled his reminder for iftar with %s minutes", userId,
							reminderTime));
					sendMessage.setText(String.format(
							"İftarlardan %s dakika önce bildirim alacaksınız. Hatırlatıcıyı kapatmak için %s komutunu kullanabilirsiniz",
							reminderTime, DISABLE_IFTAR_REMINDER_COMMAND));
				} else {
					sendMessage.setText("Lütfen pozitif bir sayı giriniz");
				}
			} catch (Exception e) {
				sendMessage.setText("Lütfen istediğiniz değeri sayı olarak giriniz");
			}
		}

		else if (userCurrentMessageText.equals(SET_IMSAK_REMINDER_COMMAND)) {

			if (user.getTownId() == null) {
				sendMessage.setText(NOT_SAVED_TOWN_ERROR);
			} else {
				sendMessage.setReplyMarkup(timeSelectionKeyboard());
				sendMessage.setText(
						"İmsaklardan kaç dakika önce hatırlatılmak istediğinizi aşağıdan seçiniz ya da sayıyla yazınız");
			}
		}

		else if (userLastMessage.equals(SET_IMSAK_REMINDER_COMMAND)) {
			try {
				int reminderTime = getReminderTime(userCurrentMessageText);

				if (reminderTime > 0) {
					service.setImsakReminderForUser(userId, reminderTime);
					logger.info(String.format("User=%s has been enabled his reminder for imsak with %s minutes", userId,
							reminderTime));
					sendMessage.setText(String.format(
							"İmsaklardan %s dakika önce bildirim alacaksınız. Hatırlatıcıyı kapatmak için %s komutunu kullanabilirsiniz",
							reminderTime, DISABLE_IMSAK_REMINDER_COMMAND));
				} else {
					sendMessage.setText("Lütfen pozitif bir sayı giriniz");
				}
			} catch (Exception e) {
				sendMessage.setText("Lütfen istediğiniz değeri sayı olarak giriniz");
			}
		}

		else {

			String foundTownName = "";
			Integer foundTownId = null;
			boolean foundExact = false;

			String clearedUserText = IftarVaktiApp.clearTurkishChars(userCurrentMessageText);

			Map<String, Integer> countryNameMap = service.getCountryNameMap();
			for (Integer countryId : countryNameMap.values()) {
				Map<String, Integer> cityNameMap = service.getCityNameMap(countryId);
				for (Integer cityId : cityNameMap.values()) {
					Map<String, Integer> townNameMap = service.getTownNameMap(cityId);
					for (Entry<String, Integer> townEntry : townNameMap.entrySet()) {
						String townName = townEntry.getKey();
						Integer townId = townEntry.getValue();

						double score = similarityService.score(townName, clearedUserText);

						if (score > 0.90) {
							foundTownName = townName;
							foundTownId = townId;

							if (score > 0.98) {
								foundExact = true;
								break;
							}
						}

						if (foundExact)
							break;
					}

					if (foundExact)
						break;
				}
			}

			if (foundTownId == null) {
				sendMessage.setText(userCurrentMessageText + " isimli şehir bulunamadı.");
			}

			else {
				String result = service.askForTown(foundTownId, foundTownName);
				sendMessage.setText(result);
			}
		}

		sendMessage.setChatId(chatId);

		try {
			execute(sendMessage);
			logger.info(String.format("User=%s with chatId=%s has been replied", userId, chatId));
		} catch (TelegramApiException e) {
			logger.error("An error occured while replying to message", e);
		}
	}

	private ReplyKeyboardMarkup mainKeyboard() {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);

		List<KeyboardRow> keyboard = new ArrayList<>();

		KeyboardRow keyboardRow1 = new KeyboardRow();
		keyboardRow1.add(ASK_REMAINING_TIME_COMMAND);

		KeyboardRow keyboardRow2 = new KeyboardRow();
		keyboardRow2.add(SAVE_TOWN_COMMAND);

		KeyboardRow keyboardRow3 = new KeyboardRow();
		keyboardRow3.add(SET_IFTAR_REMINDER_COMMAND);
		keyboardRow3.add(DISABLE_IFTAR_REMINDER_COMMAND);

		KeyboardRow keyboardRow4 = new KeyboardRow();
		keyboardRow4.add(SET_IMSAK_REMINDER_COMMAND);
		keyboardRow4.add(DISABLE_IMSAK_REMINDER_COMMAND);

		KeyboardRow keyboardRow5 = new KeyboardRow();
		keyboardRow5.add(HELP_COMMAND);

		keyboard.add(keyboardRow1);
		keyboard.add(keyboardRow2);
		keyboard.add(keyboardRow3);
		keyboard.add(keyboardRow4);
		keyboard.add(keyboardRow5);

		replyKeyboardMarkup.setKeyboard(keyboard);

		return replyKeyboardMarkup;
	}

	private ReplyKeyboardMarkup townSelectionKeyboard(Collection<String> places) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(true);

		List<KeyboardRow> keyboard = new ArrayList<>();
		replyKeyboardMarkup.setKeyboard(keyboard);

		KeyboardRow row = new KeyboardRow();
		row.add(BACK_ARROW_COMMAND);
		keyboard.add(row);

		for (String city : places) {
			row = new KeyboardRow();
			row.add(city);
			keyboard.add(row);
		}

		return replyKeyboardMarkup;
	}

	private ReplyKeyboardMarkup timeSelectionKeyboard() {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(true);

		List<KeyboardRow> keyboard = new ArrayList<>();
		replyKeyboardMarkup.setKeyboard(keyboard);

		for (int i = 1; i <= 10; i++) {
			KeyboardRow row = new KeyboardRow();
			row.add(String.valueOf(i));
			keyboard.add(row);
		}

		for (int i = 15; i <= 60; i += 5) {
			KeyboardRow row = new KeyboardRow();
			row.add(String.valueOf(i));
			keyboard.add(row);
		}
		for (int i = 75; i <= 300; i += 15) {
			KeyboardRow row = new KeyboardRow();
			row.add(String.valueOf(i));
			keyboard.add(row);
		}

		return replyKeyboardMarkup;
	}

	private int getReminderTime(String str) {
		return Integer.parseInt(str);
	}

	@Override
	public String getBotUsername() {
		return config.getBotUserId();
	}

	@Override
	public String getBotToken() {
		return config.getBotToken();
	}
}
