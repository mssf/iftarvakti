package com.kayalar.iftarvakti.http;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.kayalar.iftarvakti.model.DailyPrayTimes;

public class DiyanetPrayTimesRequester {

	public static Map<String, DailyPrayTimes> requestForTown(Integer townId)
			throws URISyntaxException, ClientProtocolException, IOException {
		URIBuilder builder = new URIBuilder(String.format("https://namazvakitleri.diyanet.gov.tr/tr-TR/%s", townId));
		HttpGet request = new HttpGet(builder.build());
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);

		int cutIndex = result.indexOf("Aylık Namaz Vakitleri</caption>");
		result = result.substring(cutIndex);

		String removeStr = "Aylık Namaz Vakitleri</caption>\r\n"
				+ "                                                            <thead>\r\n"
				+ "                                                                <tr>\r\n"
				+ "                                                                    <th>Miladi Tarih</th>\r\n"
				+ "                                                                    <th>İmsak</th>\r\n"
				+ "                                                                    <th>G&#252;neş</th>\r\n"
				+ "                                                                    <th>&#214;ğle</th>\r\n"
				+ "                                                                    <th>İkindi</th>\r\n"
				+ "                                                                    <th>Akşam</th>\r\n"
				+ "                                                                    <th>Yatsı</th>\r\n"
				+ "                                                                </tr>\r\n"
				+ "                                                            </thead>\r\n"
				+ "                                                            <tbody>";

		result = result.substring(removeStr.length());

		result = result.replaceAll("<tr>", "");
		result = result.replaceAll("</tr>", "");
		result = result.replaceAll("                                                                                ",
				"");
		result = result.replaceAll("                                                                            ", "");

		result = result.substring(0, result.indexOf("</tbody>"));
		result = result.replace("\n", "");
		result = result.replace("\r", "");
		result = result.replaceAll("<td>", "");
		result = result.replaceAll("                                                            ", "");

		Map<String, DailyPrayTimes> map = new LinkedHashMap<String, DailyPrayTimes>();

		String[] lines = result.split("</td>");

		int count = 0;

		DailyPrayTimes prayTimes = new DailyPrayTimes();

		for (String line : lines) {

			if (count % 7 == 0) {
				String dayStr = getDayStr(line);
				map.put(dayStr, prayTimes);
				prayTimes.setYear(Integer.parseInt(dayStr.substring(0, 4)));
				prayTimes.setMonth(Integer.parseInt(dayStr.substring(4, 6)));
				prayTimes.setDay(Integer.parseInt(dayStr.substring(6)));
			}

			if (count % 7 == 1) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setImsakHour(hour);
				prayTimes.setImsakMinute(minute);
			}

			if (count % 7 == 2) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setSabahHour(hour);
				prayTimes.setSabahMinute(minute);
			}

			if (count % 7 == 3) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setOgleHour(hour);
				prayTimes.setOgleMinute(minute);
			}

			if (count % 7 == 4) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setIkindiHour(hour);
				prayTimes.setIkindiMinute(minute);
			}

			if (count % 7 == 5) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setAksamHour(hour);
				prayTimes.setAksamMinute(minute);
			}

			if (count % 7 == 6) {
				int hour = Integer.parseInt(line.substring(0, 2));
				int minute = Integer.parseInt(line.substring(3));
				prayTimes.setYatsiHour(hour);
				prayTimes.setYatsiMinute(minute);

				prayTimes = new DailyPrayTimes();
			}

			count++;
		}

		return map;
	}

	private static String getDayStr(String htmlStr) {
		String result = "";
		String[] split = htmlStr.split(" ");
		result += split[2];

		if (split[1].equals("Ocak"))
			result += "01";
		else if (split[1].equals("Şubat"))
			result += "02";
		else if (split[1].equals("Mart"))
			result += "03";
		else if (split[1].equals("Nisan"))
			result += "04";
		else if (split[1].equals("Mayıs"))
			result += "05";
		else if (split[1].equals("Haziran"))
			result += "06";
		else if (split[1].equals("Temmuz"))
			result += "07";
		else if (split[1].equals("Ağustos"))
			result += "08";
		else if (split[1].equals("Eylül"))
			result += "09";
		else if (split[1].equals("Ekim"))
			result += "10";
		else if (split[1].equals("Kasım"))
			result += "11";
		else if (split[1].equals("Aralık"))
			result += "12";
		else {
			return null;
		}

		result += split[0];

		return result;
	}

}
