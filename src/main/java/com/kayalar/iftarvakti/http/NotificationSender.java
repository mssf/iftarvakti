package com.kayalar.iftarvakti.http;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NotificationSender {

	private static final String BASE_URL = "https://api.telegram.org/bot";

	private static final Logger logger = Logger.getLogger(com.kayalar.iftarvakti.http.NotificationSender.class);

	private String token;

	public NotificationSender(String token) {
		this.token = token;
	}

	public void sendMessage(SendMessage sendMessage) {

		try {
			String url = getBaseUrl() + sendMessage.getMethod();
			HttpPost httppost = new HttpPost(url);
			httppost.addHeader("charset", StandardCharsets.UTF_8.name());
			httppost.setEntity(
					new StringEntity(new ObjectMapper().writeValueAsString(sendMessage), ContentType.APPLICATION_JSON));
			sendHttpPostRequest(httppost);
		} catch (Exception e) {
			logger.error("An error occured while sending notification to user", e);
		}

	}

	private void sendHttpPostRequest(HttpPost httppost) throws IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		try (CloseableHttpResponse response = httpClient.execute(httppost)) {
		} catch (Exception e) {
			logger.error("An error occured while sending notification to user", e);
		} finally {
			httpClient.close();
		}
	}

	public String getBaseUrl() {
		return BASE_URL + token + "/";
	}
}
